# Blinien aika {#bliniaika}

## Blinit {#blinit}

Monet ruokalajit ovat ohjelmistoomme päätyneet Dollen ja Pertin kautta, niin blinitkin. Käyttämäni ohjekin on Dollen Ruokavuosi ja muita reseptejä -kirjasta. Ne ovat aina olleet alkutalven, usein hiihtoloman ajan ruokalaji. Muistan, että ainakin Loviisan mökillä niitä laitettiin juuri hiihtolomalla. Pojat söivät blinejä aluksi omilla täytteillä, mm. katkaravuilla, mutta myöhemmin ovat kyllä erilaiset mädit maistuneet!

```{r echo=FALSE}
knitr::include_graphics("photos/07-blinit.jpg")
```

Dollen ohje on tosi isolle porukalle, joten panen tähän sen, jota käytin mm. vuoden 2021 bliniateriaan; taikinaa jää aina annettavaksi mukaan Santulle/Jopelle ja vielä muutamaan bliniin Ripalle ja minulle seuraavana päivänä.

#### Ainekset {.unnumbered}

-   1 l vettä
-   60 g hiivaa
-   350 g tattarijauhoja
-   550 g vehnäjauhoja
-   2 tl suolaa
-   3 munankeltuaista
-   1,1 l maitoa
-   puoli pulloa pilsneriä
-   4 munanvalkuaista
-   Paistamiseen voi-öljyseosta

Liuota hiiva haaleaan veteen. Lisää jauhot, anna seistä ja kohota yön yli. Peitä esimerkiksi kelmulla. (Minulla on iso punainen elintarvikemuovista tehty kannellinen ämpäri, jossa olen aina taikinan tehnyt.) Seuraavana aamuna lämmitä maito mikrossa haaleaksi, vatkaa taikinan sekaan. Lisää keltuaiset. Jätä valkuaiset jo toiseen astiaan myöhemmin vatkattaviksi. Lisää suola. 1--2 tuntia ennen paistamista: vatkaa valkuaiset vaahdoksi. Lisää pilsneri taikinaan. Lisää valkuaisvaahto.

Blinien paisto on aina aluksi vähän oikean lämpötilan ja rasvan määrän hakua, joskus ensimmäiset palavat, mutta kun blinipannut rasvoittuvat hyvin ja oikea paistolämpötila löytyy, homma alkaa sujua.

Koronan varjossa vuoden 2020 blinikestit jäivät pitämättä, mutta 27.2.2021 kestit päätettiin Purokujalla pitää. Intouduin perjantaina siivoamaan kodinhoitohuonetta vähän perusteellisemmin ja tein blinejä varten lohimoussen (ohje mukana). Ripa oli ostanut iltapalaksemme sinisimpukoita, jotka vielä jouduin käymään läpi "partojen" poistamiseksi. Kun sitten sinisimpukat oli syöty, siirryimme Netflixin ääreen ja unohdin blinitaikinan tekemisen kokonaan! Onneksi puoli kahdentoista aikaan nukkumaan mennessä havahduin unohdukseen ja sain taikinan tekeytymään. Ripa nauraa hihitteli ärtymykselleni, otti minusta kuvan työn touhusssa ja pani sen Valveet-WhatsAppiin: "Mumma muisti hetki sitten, että blinitaikina piti valmistella." Klo 23.30 tuli Santulta viesti: "No justiinsa. Täällä pojat nukkuneet jo varmaan tunnin. Hyvin pärjätään lämpötilan puolesta." Huomionarvoista on se, että Santun viestin "täällä" tarkoitti Nuuksiota, jossa Santtu oli poikien kanssa telttaretkellä!

Lauantaiaamuna jatkoin taikinan valmistelua ennen kuin lähdin Sihtikujalle Myllypuroon Pirkon seuraksi siksi aikaa, kun Pio oli pitämässä luistelijoiden ohjaustunteja. Pirkko ei enää tule yksin toimeen tuntia paria kauempaa ja Jukka on ollut jo yli kuukauden sairaalassa, jossa perusvaivojensa lisäksi sai keuhkokuumeen ja vielä päälle koronan! Onneksi näyttäisi nyt olevan toipumassa. Joppe viimeisteli taikinan paistokuntoon ja valmisti herkullisen tonnikalatahnan lisukkeeksi sekä teki perinteisiin nykyään kuuluvan lettutaikinan lapsia varten, Maria sen sitten paistoi. Blinien paistosta huolehti Santtu Johannan kanssa.

Lisukkeina oli siis muikunmätiä, kylmäsavustettua kirjolohenmätiä, tonnikalatahna ja lohimousse. Täyssmetanan sijaan olen nykyään alkanut käyttää 20-prosenttista, maistuu ihan hyvältä ja on vähän terveellisempää. Tällä kertaa ei ollut sienisalaattia. Viime vuosina olen ottanut mukaan myös suolakurkut hunajan ja smetanan kanssa, ne maistuvat erityisesti Lassille, joka ei ainakaan tänä vuonna blineistä perustanut. Pessi kyllä pisteli yhden kokonaisen, taisi mennä ilman lisukkeita. Lydia innostui maistamaan mätiä, mutta totesi, että "maistuu kalalle"; pisteli sitten poskeensa lohimoussea oikein urakalla. Perusletut maistuivat kyllä kaikille lapsille, Eeliskin sai elämänsä ensimmäisen letun.

## Lohimousse {#lohimousse}

([HS Kokeile tätä -palstalta, Katja Bäcksbacka](https://www.hs.fi/ruoka/reseptit/art-2000002567495.html))

Tämä ohje voisi olla pääsiäisen, vapun, juhannuksen, joulun tai minkä vain yhteydessä, sopii mihin aikaan vain. Taisi alun perin tuo ohje olla jonain vuonna joulun tienoilla Hesarissa.

#### Ainekset {.unnumbered}

-   noin 200 g graavisuolattua kirjolohta
-   noin 200 g kylmäsavustettua kirjolohta
-   2 sipulia
-   2 dl kuohukermaa
-   1 tlk rahkaa
-   1 tlk kermaviiliä
-   1 rkl sokeria
-   1 tl sitruunapippuria
-   ¼ tl mustapippuria
-   ½ -- 1 dl silputtua tilliä
-   noin ¾ tl suolaa

Leikkaa lohet pieniksi kuutioiksi, jätä muutama viipale koristeeksi. Silppua sipulit hyvin hienoksi. Vatkaa kuohukerma löysähköksi vaahdoksi. Sekoita rahkan joukkoon kermaviili, lohikuutiot, sipulisilppu ja mausteet. Sekoita ainekset keskenään. Lisää lopuksi kermavaahto varovasti nostellen. Tarkista maku ja lisää arvittaessa mausteita. Anna maustua jääkaapissa vähintään muutaman tunnin ajan ennen tarjoamista. Kirjolohen voi vaihtaa loheksi, kuohukerman vähemmän rasvaiseen. Annoksesta tulee hyvin riittoisa.
