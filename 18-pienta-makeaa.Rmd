# Pientä makeaa

## Suklaapuuro {#suklaapuuro}

(T-kuluttajapalvelun Ruokavinkit Lokakuu 1983)

#### Ainekset {.unnumbered}

-   1 l maitoa
-   1 ½ dl sokeroitua kaakaojauhetta
-   1 ¼ dl mannasuurimoita
-   (koristeeksi suklaarouhetta)

Kuumenna maito kiehuvaksi. Vispaa joukkoon kaakao ja suurimot. Keitä hiljalleen noin viisi minuuttia välillä sekoittaen. (Mausta halutessasi suklaarouheella. Tarjoa maidon kera.)

Tätä puuroa on vuosien varrella keitelty monelle pienelle syöjälle. On tällä hetkelläkin Lassin ja Pessin mieliruokaa Purokujalla.

## Pannukakku {#pannukakku}

#### Ainekset {.unnumbered}

-   4 munaa
-   2 dl kermaa
-   5 dl vehnäjauhoja
-   1 tl suolaa

Kaikki sekaisin kulhossa ja pellille leivinpaperin päälle paistumaan.

Tämä pannariohje on peräisin eräältä Iivisniemen tuttavalta Santerin alakoulun ajoilta.

## Suklaajäädyke {#suklaajaadyke}

(Ohjeen lähde ei tiedossa)

#### Ainekset {.unnumbered}

-   2 dl kuohu- tai vispikermaa
-   2 rkl sokeria
-   2 munaa
-   noin 150 g sulatettua suklaata

Vatkaa kerma ja 1 rkl sokeria vaahdoksi. Vatkaa munat ja 1 rkl sokeria vaahdoksi. Yhdistä vaahdot. Lisää joukkoon sulatettu tai rouhittu suklaa. Kaada seos jäädyke- tai kakkuvuokaan ja laita pakastimeen vähintään yön yli. Kumoa tarjoiluvadille ja koristele mielesi mukaan.

## Pehmeänmakuinen raparperitorttu {#raparperitorttu}

(Ohjeen lähde ei tiedossa, ilmeisesti Hesarista)

#### Ainekset {.unnumbered}

-   100 g margariinia
-   3 dl vehnäauhoja
-   ½ dl sokeria
-   1 muna

Täyte:

-   4 dl pieniä raparperipaloja
-   2 munaa
-   2 dl kermaa
-   ¾ dl sokeria

Sekoita jauhot ja pehmeä rasva. Lisää sokeri ja muna. Sekoita taikina tasaiseksi ja painele se voidellun matalareunaisen vuoan pohjalle ja reunoille. Levitä raparperipalat taikinalle.

Vatkaa sekaisin munat, sokeri ja kerma, mutta älä vaahdota. Kaada seos raparperipalojen päälle.

Paista torttua 200-asteisessa uunissa noin 45 minuuttia eli kunnes raparperi-munaseos on hyytynyt ja saanut kauniin värin. Kypsymisaika riippuu torttuvuoan laakeudesta.

## Pellillinen omenapiirakkaa {#omenapiirakka}

(Alkuperä hämärä)

Omenapiirakkaa on aikanaan tullut tehdyksi hyvin monella reseptillä; tämä kannattaa panna muistiin helppoutensa takia.

#### Ainekset {.unnumbered}

-   4 munaa
-   3 dl sokeria
-   2 tl leivinjauhetta
-   4 dl vehnäjauhoja
-   1,5 dl maitoa
-   1 dl rasvaa (öljy/margariini/voi)
-   omenia viipaleina
-   sokeria, kanelia

Kuumenna uuni, 225 astetta. Vatkaa munat ja sokeri kuohkeaksi. Lisää keskenään sekoitetut kuivat aineet, sitten maito ja öljy.

Levitä taikina leivinpaperille, lado päälle runsaasti omenaviipaleita. Ripottele päälle sokeria ja kanelia. Paista uunissa noin 15 minuuuttia.

(Tarjoa lisänä vaniljajäätelöä tai vaniljakastiketta.)

## Helpoista helpoin mustikkapiirakka {#mustikkapiirakka}

(oma kehitelmä)

-   lasillinen kananmunia
-   lasillinen sokeria
-   lasillinen vehnäjauhoja
-   mustikoita

Vatkaa munat ja sokeri hyväksi vaahdoksi. Lisää vehnäjauhot.

Levitä taikina leivinpaperille pellille. Lisää taikinaan mustikat; määrän voi itse päättää.

Paista n. 180--200-asteisessa uunissa, kunnes kauniin väristä.

Tämä piirakka valmistuu tosi helposti ja nopeasti; on kelvannut hyvin kaikille lapsille. Lisukkeeksihan voi tarjota jäätelöä jne.

## Mustikka-marenkijäädyke {#mustikka-marenki}

([Yhteishyvä.fi](https://yhteishyva.fi/reseptit/mustikka-marenkijaadyke/recipe-1277))

#### Ainekset {.unnumbered}

-   8 annosta
-   1 kananmuna
-   0,5 dl sokeria
-   2 dl kuohukermaa
-   200 g pakastemustikoita
-   1 pussi (60 g) marenkeja
-   koristeluun pakastemustikoita

Vatkaa muna ja sokeri vaahdoksi. Vatkaa kerma vaahdoksi. Sulata ja soseuta mustikat ja lisää munavaahtoon. Kääntele vaahdot keskenään. Murskaa marengit kevyesti ja sekoita joukkoon.

Kaada seos runsaalla tuorekelmulla vuorattuun pitkulaiseen vuokaan ja peitä foliolla. Pakasta yön yli. Nosta jäädyke tarjolle kelmun avulla noin 15 minuuttia ennen tarjoilua.
