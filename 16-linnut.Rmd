# Linnut

## Paistetut ankankoivet {#ankankoivet}

(www.reininliha.fi)

#### Ainekset {.unnumbered}

(ohje 4:lle)

-   6 ankankoipea

Marinadi:

-   2 dl öljyä
-   1 dl sitruunamehua
-   2 rkl hienonnettua timjamia
-   2 rkl hienonnettua lehtipersiljaa
-   1 rkl hienonnettua rosmariinia
-   1 rkl hienoa suolaa
-   vastajauhettua mustapippuria

Sekoita marinadiaineet ja anna koipien maustua tiiviissä pussissa jääkaapissa vuorokauden ajan. (Kääntele pussia, että maustuu tasaisesti.) Valuta ylimääräinen marinadineste koivista pois. Kuumenna pata kuumaksi ja ruskista koivet yrttien kera ensin nahkapuoli pannua vasten, sitten lihapuoli. Jatka kypsennystä 150-asteisessa uunissa siten, että nahkapuoli on ylöspäin. Koivet eivät saa olla päällekkäin. (Vaikea toteuttaa, jos koipia on 6 tai enemmän!) Kypsymisaika on noin 2 tuntia. Liha on kypsää, kun se tuntuu pistäessä (hammastikulla) pehmeältä. (Jos ei käytä kertynyttä rasvaa muuhun, se kannattaa panna pussissa roskikseen, ei kannata panna viemäriin.)

## Paistettu sorsa {#paistettusorsa}

Korjusalmella syötiin paljon sorsalintuja, koska isä oli innokas metsästäjä, joka tosin viimeisinä vuosinaan lopetti metsästyksen kokonaan. Pertti ja Hannu olivat metsästyksessä paljon mukana, minäkin pääsin joskus mukaan veneeseen, jota "puukittiin" tiheässä kaislikossa, joskus juututtiin kiinnikin. Hannu ei metsästykseen aina tarvinnut edes pyssyä: yhdellä marjastusreissulla Hannu otti juoksemalla linnun kiinni, olisikohan silloin ollut sorsa tai teeri! Minä olin kovin innostunut sorsien puhdistamisesta, olin ylpeä, kun sain vähitellen tehdä koko operaation, ei vain höyhenten poistoa. Totuin myös sorsien ruoaksi laittamiseen. Muistan, että jossain vaiheessa olin sorsastusaikaan Liperissä Pertin ja Armilan Pekan kanssa niin, että minä valmistin yksin sorsa-aterian padassa heidän metsästyssaaliistaan.

Muistelen, että sorsat valmistettiin kokonaisina padassa, liemenä oli varmaankin vettä ja kermaa; en muista, pantiinko mukaan kasviksia. Voisin kuvitella, että sieniä oli mukana. Kun olen myöhemmin tehnyt sorsaa, olen myös valmistanut sen padassa paloiteltuna, mukaan olen pannut kasviksia/juureksia, sieniä. Haudutusliemi on ollut lähinnä punaviiniä. Nykyisinhän sorsan valmistaminen on sikäli helppoa, että sorsia saa ihan patavalmiina.

Panen tähän Maa- ja kotitalousnaisten ohjeen Haudutettua sorsaa kastikkeessa, koska se muistuttaa mielestäni sekä Korjusalmen versiota että myöhemmin soveltamaani.

## Haudutettua sorsaa kastikkeessa {#haudutettusorsa}

([www.maajakotitalousnaiset.fi](https://www.maajakotitalousnaiset.fi/reseptit/haudutettua-sorsaa-kastikkeessa-5618))

#### Ainekset {.unnumbered}

(4 annosta)

-   4 pientä sorsalintua (minusta 2 normaalikokoista heinäsorsaa riittää neljälle)
-   suolaa
-   paprikaa, rakuunaa, basilikaa
-   vehnäjauhoja
-   tilkka öljyä
-   4 dl punaviiniä
-   2 sipulia
-   2 dl tuoreita herkkusieniä, kantarelleja tai suppilovahveroita
-   1 porkkana (minä panisin myös palsternakkaa, juuriselleriä...)
-   (2 rkl tomaattisosetta)
-   4 dl vettä

Paloittele sorsat. Irrota rinnat ja koivet. Pilko selkärangat kahteen kolmeen palaan. Mausta lintupalat ja luut suolalla, paprikalla ja yrteillä. Jauhota lihat ja luut. Kuumenna paistinpannussa öljytilkka. Ruskista lintupalat hiljalleen.

Siirrä palat kattilaan. Kaada viini kattilaan, jossa on sorsapalat ja lisää vettä niin, että lihapalat peittyvät. Anna sorsapalojen kypsyä hiljalleen noin tunti. Liha on kypsää, kun se irtoaa luista helposti.

Lihojen kypsymisen loppupuolella kuori ja hienonna sipulit, suikaloi porkkana (ja muut juurekset). Puhdista ja hienonna sienet. Kypsennä hetken aikaa sieniä ja vihanneksia paistinpannulla. (Lisää joukkoon tomaattisose.) Jatka kypsentämistä minuutin ajan. Kaada pannulle tilkka vettä.

Ota sorsapalat kattilasta ja poista niistä luut. Laita lihat takaisin kattilaan. Kaada kasvisliemi lihojen päälle kattilaan. Jatka kypsentämistä noin 15 minuuttia.

Ruskistusvaiheessa käyttäisin valurautapataani, johon sitten myöhemmin lisäisin myös sipulit jne. En taitaisi myöskään ruveta luita poistamaan, mutta olisihan se tietysti syöjäystävällistä. Loppuvaiheessa voisin lorauttaa pataan vielä tilkan konjakkia tai madeiraa.

## Paistettu hanhi {#hanhi}

(Ruutusarjan keittokirja, s. 238)

Tämä ruoka löysi meille tiensä, kun Pertti aina puhui Martinpäivän (10.11.) hanhesta. Meillä se oli sitten usein isänpäivän ruoka, koska isänpäivä osuu noille hujakoille. Lutheriin ja kekriinhän se on alun perin liittynyt. Ostin hanhen mieluiten tuoreena, Hesan Kauppahallista mm. Hintaa sille tuli! Nyt ei ole enää aikoihin hanhea paisteltu, yksi ei enää riitä isolle ruokakunnallemme, joten hinta kipuaisi jo pilviin. Mutta hyväähän se oli!

Hanhi puhdistetaan, huuhdotaan (ja reisien jänteet vedetään pois. On taidettu nykyään jo valmiiksi poistaa. Tarkista, onko vatsaonteloon pantu kaula ja kivipiira, joita minä en kokkauksessani jaksanut hyödyntää.) Se hierotaan sisä- ja ulkopuolelta sitruunalla, suolalla, mustapippurilla ja inkiväärillä. (Yrteistä timjami sopii hyvin.) Hanhi täytetään kuorituilla omenanpuolikkailla ja kivettömillä luumuilla. Hanhi ommellaan kiinni ja sidotaan. Se pannaan rintapuoli ylöspäin ritilälle uunipannulle. Paistetaan uunissa (175 astetta) 2 ½--3 tuntia. Reiteen pistämällä voi kokeilla, onko hanhi kypsä. Jos lihasneste on kirkasta ja väritöntä, on hanhi valmis. Hanhen päälle valellaan pari lusikallista kylmää vettä ja uuninluukun annetaan olla raollaan viimeiset 15 minuuttia, että nahka saadaan kiiltäväksi ja rapeaksi.

Tarjoillaan luumujen ja vastakeitettyjen omenanpuolikkaiden, punakaalin ja keitettyjen perunoiden kanssa. Omenanpuolikkaat voidaan täyttää mandariineilla ja rusinoilla ja päälle kaadetaan öljy-viinietikkakastiketta. Muistanpa tämän viimeistelynkin jonkin kerran tehneeni.

## Coq au vin {#coqauvin}

(ei merkintää ohjeen alkuperästä)

#### Ainekset {.unnumbered}

-   noin 2 broileria (joskus olen käyttänyt koipi-reisipaloja ja paistileikkeitä)
-   1 dl vehnäjauhoja
-   suolaa, mustapippuria
-   öljyä (voita)
-   200 g herkkusieniä (tai muita sieniä)
-   8--10 pientä salottisipulia
-   1 pkt (140g) pekonia
-   2 valkosipulinkynttä
-   3 rkl konjakkia
-   noin 5 dl punaviiniä
-   1 laakerinlehti
-   4 oksaa tuoretta timjamia
-   4 dl kanalientä
-   persiljaa

Paloittele broileri, kuivaa, pyörittele vehnäjauhoissa. Ruskista, mausta suolalla ja pippurilla, nosta pataan.

Harjaa herkkusienet, pilko tarvittaessa. Kuori sipulit. Hienonna pekoni. Paahda pannulla. Lisää kokonaiset sipulit ja hienonnetut valkosipulinkynnet. Kääntele hetki. Kaada pekoni-sipuliseos pataan. Paahda myös sienet, lisää pataan.

Lisää pannulle konjakki ja punaviini, anna kiehahtaa. Valuta pataan. Lisää laakerinlehti ja timjaminoksat. Kaada päälle kanaliemi. Paista 150 asteessa noin 1,5 tuntia.

Dolle ohjaa omassa reseptissään (Ruokavuosi ja muita reseptejä s. 45) peräti liekittämään konjakin, mikä on minulle kuitenkin liian uskaliasta ja mutkallista!

## Täytetyt viiriäiset {#viiriaiset}

(www.mainio.net)

Viiriäiset löytyivät ruokavalioomme joltain ulkomaanmatkalta, todennäköisesti Espanjasta. Koska ne ovat pieniä ja ohjeet sisältävät runsaasti yksityiskohtia, ne ovat aika hitaita ja vaivalloisia valmistaa, mutta jostain syystä olen hyvin jaksanut niiden kanssa askarrella; no, syy on tietysti herkullinen maku!

#### Ainekset {.unnumbered}

-   4 (luuttomaksi puhdistettua) viiriäistä
-   1 fasaanin rinta (myös broileri käy)
-   60 g hienonnettua siitaketta (tai muuta sientä)
-   1 hienonnettu sipuli
-   50 g paahdettuja pinjansiemeniä
-   2 timjaminoksaa
-   1 rkl mustaviinimarjahyytelöä
-   suolaa, mustapippuria myllystä
-   voita kuullotukseen

Kuullota sipuli, siitake, timjami ja pinjansiemenet voissa. Jauha fasaani tehosekoittimessa ja sekoita kuullotettujen kasvisten kanssa. Mausta suolalla, pippurilla, hyytelöllä ja jäähdytä. Levitä viiriäiset pöydälle nahkapuoli alaspäin ja mausta suolalla ja pippurilla. Jaa täyte keoksi lintujen sisään ja sulje huolellisesti hammastikulla. Pariloi linnut nopeasti kuumalla pannulla ruskeaksi. Paista lintuja 185-asteisessa uunissa noin 18 minuuttia. Anna vetäytyä pyyhkeen alla 5--10 minuuttia.

Tätä fasaanitäytteistä versiota olen kerran tarjonnut Pertille ja Liisalle. Erilaisia variaatioita olen tehnyt monenlaisia, jolloin olen käyttänyt mitä tahansa saatavilla olevia pähkinöitä (hienonnettuina), joskus olen pannut täytteeseen mm. maksapateeta. Yhden tällaisen version tein Sardinian vuokratalossa, jossa viivyimme viikon verran yhdellä pyörämatkallamme.

## Paistettua helmikananrintaa {#helmikana}

(Stockmannin ohje)

#### Ainekset {.unnumbered}

(2:lle)

-   2 helmikananrintaa
-   1 rkl tuoretta rakuunaa
-   1 rkl tuoretta salviaa
-   2 rkl voita
-   suolaa, mustapippuria

Hienonna yrtit ja työnnä kananrintojen nahan alle. Mausta rinnat suolalla ja pippurilla.

Paista kananrinnat kuumalla pannulla kauniin kullanruskeiksi. Kypsennä loppuun 150-asteisessa uunissa, kunnes sisälämpötila 73 astetta; noin 15 minuuttia.

Tätä ruokaa syömme Ripan kanssa tosi usein sunnuntaisin. Pakkaseen on helppo varata siipiluullista helmikananrintaa; kunhan muistaa ottaa ajoissa sulamaan, siitä saa nopeasti herkkuaterian, jonka lisukkeeksi sopivat erityisen hyvin sienet.

## Fasaanipaisti {#fasaani}

(Ranska Hyvän ruoan maa, Stockmann 1997)

#### Ainekset {.unnumbered}

(6:lle)

-   2 kokonaista fasaania tai broileria
-   100 g kylmäsavustettua pekonia, pieneksi kuutioituna
-   suolaa ja mustapippuria
-   rosmariinia, timjamia ja 3 persiljanoksaa
-   nokare voita
-   4 dl vahvaa Touch of Taste -liha- tai kanalientä (siis fondia)
-   2 dl valkoviiniä
-   ½ sitruunan raastettu kuori
-   1 tl hunajaa

Beurre manie´:

-   1 rkl voita
-   1 rkl vehnäjauhoja tahnaksi sekoitettuna

Lisäksi:

-   3 prk pieniä, ranskalaisia Jean Nicolas -herneitä (ehkä jokin muukin merkki käy!)
-   20 kypsennettyä pikkusipulia
-   Paistettuja pikkuperunoita

Paloittele kokonaiset fasaanit neljään osaan. Ruskista pekoni, lisää joukkoon voita ja paista fasaaninpalaset kullanruskeiksi. Siirrä ne laakeaan uunivuokaan. Suolaa ja pippuroi liha ruskistamisen jälkeen.

Huuhtele paistinpannu lihaliemellä ja kaada vuokaan makua antavaksi liemeksi. Lisää mausteeksi yrtit, raastettu sitruunankuori, valkoviini ja hunaja. Nosta vuoka 180-asteiseen uuniin 2 tunniksi. (Broileri kypsyy nopeammin.) Voit lisätä vuokaan vielä loppuvaiheessa valutetut pikkuherneet ja sipulit.

Tein tästä broileriversion pari vuotta sitten Hirvensalmella, jossa olimme Pertin vieraina Hannun ja Kirsin kanssa. Muistan, että ruoasta tuli kertakaikkisen hyvää.
